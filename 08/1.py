import urllib.request
import os

def get_input() -> str:
    req = urllib.request.Request("https://adventofcode.com/2022/day/8/input")
    req.add_header("Cookie", os.environ["ADVENT_COOKIE"])
    res = urllib.request.urlopen(req)
    encoding = res.headers.get_content_charset("utf-8")
    input = res.read().decode(encoding)
    return input

def is_more_or_equal_above(i: int, j: int, lines: list[str]) -> bool:
    for x in range(i):
        if lines[x][j] >= lines[i][j]:
            return True
    return False
def is_more_or_equal_below(i: int, j: int, lines: list[str]) -> bool:
    for x in range(i+1, len(lines)):
        if lines[x][j] >= lines[i][j]:
            return True
    return False
def is_more_or_equal_left(i: int, j: int, lines: list[str]) -> bool:
    for x in range(j):
        if lines[i][x] >= lines[i][j]:
            return True
    return False
def is_more_or_equal_right(i: int, j: int, lines: list[str]) -> bool:
    for x in range(j+1, len(lines)):
        if lines[i][x] >= lines[i][j]:
            return True
    return False

lines = get_input().splitlines()
num_rows = len(lines)
count = 0
for i in range(1, num_rows-1):
    for j in range(1, num_rows-1):
        if not is_more_or_equal_above(i, j, lines) \
            or not is_more_or_equal_below(i, j, lines) \
            or not is_more_or_equal_left(i, j, lines) \
            or not is_more_or_equal_right(i, j, lines):
            count+=1
            continue

num_outside = num_rows*4-4
print(count + num_outside)