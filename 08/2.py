import urllib.request
import os

def get_input() -> str:
    req = urllib.request.Request("https://adventofcode.com/2022/day/8/input")
    req.add_header("Cookie", os.environ["ADVENT_COOKIE"])
    res = urllib.request.urlopen(req)
    encoding = res.headers.get_content_charset("utf-8")
    input = res.read().decode(encoding)
    return input

def above(i: int, j: int) -> int:
    count = 0
    for x in reversed(range(i)):
        count+=1
        if lines[x][j] >= lines[i][j]:
            break
    return count
def below(i: int, j: int) -> int:
    count = 0
    for x in range(i+1, num_rows):
        count+=1
        if lines[x][j] >= lines[i][j]:
            break
    return count
def left(i: int, j: int) -> int:
    count = 0
    for x in reversed(range(j)):
        count+=1
        if lines[i][x] >= lines[i][j]:
            break
    return count
def right(i: int, j: int) -> int:
    count = 0
    for x in range(j+1, num_rows):
        count+=1
        if lines[i][x] >= lines[i][j]:
            break
    return count

lines = get_input().splitlines()
num_rows = len(lines)
max = 0
for i in range(1, num_rows-1):
    for j in range(1, num_rows-1):
        cur = above(i, j) * below(i, j) * left(i, j) * right(i, j)
        if cur > max:
            max = cur

print(max)