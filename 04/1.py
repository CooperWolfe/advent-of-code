import urllib.request
import os

def get_input() -> str:
    req = urllib.request.Request("https://adventofcode.com/2022/day/4/input")
    req.add_header("Cookie", os.environ["ADVENT_COOKIE"])
    res = urllib.request.urlopen(req)
    encoding = res.headers.get_content_charset("utf-8")
    input = res.read().decode(encoding)
    return input

count = 0
for line in get_input().splitlines():
    # Split line into 2 [min, max]'s
    assmts = [[int(v2) for v2 in v1.split("-")] for v1 in line.split(",")]
    # Sort by offset asc, them limit desc
    assmts.sort(key=lambda x: (x[0], x[0] - x[1]))
    # [0] contains [1] if [0].min <= [1].min and [0].max >= [1].max
    # The only scenario in which [1] contains [0] after above sort is if [0] == [1]
    if assmts[0][0] <= assmts[1][0] and assmts[0][1] >= assmts[1][1]:
        count+=1

print(count)