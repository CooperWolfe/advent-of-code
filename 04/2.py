import urllib.request
import os

def get_input() -> str:
    req = urllib.request.Request("https://adventofcode.com/2022/day/4/input")
    req.add_header("Cookie", os.environ["ADVENT_COOKIE"])
    res = urllib.request.urlopen(req)
    encoding = res.headers.get_content_charset("utf-8")
    input = res.read().decode(encoding)
    return input

count = 0
for line in get_input().splitlines():
    # Split line into 2 [min, max]'s
    assmts = [[int(v2) for v2 in v1.split("-")] for v1 in line.split(",")]
    # There is overlap if the total range < the sum of ranges
    if max(assmts[0][1], assmts[1][1])-min(assmts[0][0], assmts[1][0])+1 < (assmts[0][1]-assmts[0][0]+1) + (assmts[1][1]-assmts[1][0]+1):
        count+=1

print(count)