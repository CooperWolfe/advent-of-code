import urllib.request
import os
import typing

TNode = typing.TypeVar("TNode", bound="Node")
class Node:
    def __init__(self, row: int, col: int, height: int, steps_to_end:int=9999999):
        self.row = row
        self.col = col
        self.height = height
        self.steps_to_end = steps_to_end
    def surrounding_nodes(self) -> typing.Iterator[TNode]:
        if self.row > 0:
            yield grid[self.row-1][self.col]
        if self.row < len(grid)-1:
            yield grid[self.row+1][self.col]
        if self.col > 0:
            yield grid[self.row][self.col-1]
        if self.col < len(grid[0])-1:
            yield grid[self.row][self.col+1]
    def travel(self, frm: TNode) -> bool:
        if frm.steps_to_end > self.steps_to_end + 1 and frm.height >= self.height-1:
            frm.steps_to_end = self.steps_to_end + 1
            return True
        else:
            return False


def get_input() -> str:
    req = urllib.request.Request("https://adventofcode.com/2022/day/12/input")
    req.add_header("Cookie", os.environ["ADVENT_COOKIE"])
    res = urllib.request.urlopen(req)
    encoding = res.headers.get_content_charset("utf-8")
    input = res.read().decode(encoding)
    return input

grid: list[list[Node]] = []
starts: list[Node] = []
end: Node
lines = get_input().splitlines()
for i in range(len(lines)):
    grid.append([])
    for j in range(len(lines[i])):
        match lines[i][j]:
            case "S" | "a":
                starts.append(Node(i, j, height=ord("a")))
                grid[i].append(starts[-1])
            case "E":
                end = Node(i, j, height=ord("z"), steps_to_end=0)
                grid[i].append(end)
            case _:
                grid[i].append(Node(i, j, height=ord(lines[i][j])))

paths: list[Node] = [end]
while len(paths) > 0:
    cur = paths.pop(0)
    for node in cur.surrounding_nodes():
        if cur.travel(frm=node):
            paths.append(node)

print(min([start.steps_to_end for start in starts]))