import urllib.request
import os

def get_input() -> str:
    req = urllib.request.Request("https://adventofcode.com/2022/day/14/input")
    req.add_header("Cookie", os.environ["ADVENT_COOKIE"])
    res = urllib.request.urlopen(req)
    encoding = res.headers.get_content_charset("utf-8")
    input = res.read().decode(encoding)
    return input

paths: list[list[tuple[int, int]]] = []
lines = get_input().splitlines()
for line in lines:
    paths.append([(int(pair.split(",")[0]), int(pair.split(",")[1])) for pair in line.split(" -> ")])

min_x = min([min([pair[0] for pair in path]) for path in paths])
max_x = max([max([pair[0] for pair in path]) for path in paths])
max_y = max([max([pair[1] for pair in path]) for path in paths])
paths = map(lambda path: list(map(lambda pair: (pair[0]-min_x, pair[1]), path)), paths)
grid: list[list[str]] = []
for i in range(max_y+1):
    grid.append([])
    for j in range(max_x-min_x+1):
        grid[i].append(".")

for path in paths:
    for x in range(len(path)-1):
        start, end = path[x], path[x+1]
        if start[0] != end[0]:
            for i in range(abs(start[0]-end[0])+1):
                grid[start[1]][min([start[0], end[0]])+i] = "#"
        if start[1] != end[1]:
            for i in range(abs(start[1]-end[1])+1):
                grid[min([start[1], end[1]])+i][start[0]] = "#"
del paths

def next_unoccupied_pos(pos: tuple[int, int]) -> tuple[int, int] | None:
    x, y = pos[0], pos[1]
    while True:
        if y+1 >= len(grid):
            return None
        if grid[y+1][x] == ".":
            y+=1
        else:
            return (x, y)

count = 0
sand_pos = (500-min_x, 0)
while sand_pos != None:
    sand_pos = (500-min_x, 0)
    while True:
        sand_pos = next_unoccupied_pos(sand_pos)
        if sand_pos == None:
            break
        if sand_pos[0] == 0:
            sand_pos = None
            break
        if grid[sand_pos[1]+1][sand_pos[0]-1] == ".":
            sand_pos = (sand_pos[0]-1, sand_pos[1]+1)
            continue
        if sand_pos[0] == len(grid[0])-1:
            sand_pos = None
            break
        if grid[sand_pos[1]+1][sand_pos[0]+1] == ".":
            sand_pos = (sand_pos[0]+1, sand_pos[1]+1)
        else:
            grid[sand_pos[1]][sand_pos[0]] = "o"
            count+=1
            break

print(count)

# for row in grid:
#     str = ""
#     for col in row:
#         str += col
#     print(str)