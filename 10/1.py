import urllib.request
import os

class Node:
    def __init__(self, cycle, next):
        self.cycle = cycle
        self.next = next

def get_input() -> str:
    req = urllib.request.Request("https://adventofcode.com/2022/day/10/input")
    req.add_header("Cookie", os.environ["ADVENT_COOKIE"])
    res = urllib.request.urlopen(req)
    encoding = res.headers.get_content_charset("utf-8")
    input = res.read().decode(encoding)
    return input

head = None
cycle = 220
while cycle > 0:
    head=Node(cycle, next=head)
    cycle-=40

x = 1
cycle = 0
lines = get_input().splitlines()
sum = 0
for line in lines:
    if line == "noop":
        cycle+=1
    elif line.startswith("addx"):
        cycle+=2
    if head != None and cycle >= head.cycle:
        sum += head.cycle * x
        head = head.next
    if line.startswith("addx"):
        x += int(line[5:])

print(sum)