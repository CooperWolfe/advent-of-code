import urllib.request
import os

def get_input() -> str:
    req = urllib.request.Request("https://adventofcode.com/2022/day/10/input")
    req.add_header("Cookie", os.environ["ADVENT_COOKIE"])
    res = urllib.request.urlopen(req)
    encoding = res.headers.get_content_charset("utf-8")
    input = res.read().decode(encoding)
    return input

class State:
    def __init__(self):
        self.x = 1
        self.cycle = 0
        self.crt = 0
        self.line = ""
    def draw(self):
        if self.crt+1 >= self.x and self.crt+1 <= self.x+2:
            self.line += "#"
        else:
            self.line += "."
        self.cycle+=1
        self.crt = (self.crt + 1) % 40
        if self.crt == 0:
            print(self.line)
            self.line = ""
    def addx(self, amt: int):
        self.x += amt

state = State()
input = get_input().splitlines()
for cmd in input:
    state.draw()
    if cmd.startswith("addx"):
        state.draw()
        state.addx(int(cmd[5:]))
