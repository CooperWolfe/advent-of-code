import urllib.request
import os

def get_input() -> str:
    req = urllib.request.Request("https://adventofcode.com/2022/day/3/input")
    req.add_header("Cookie", os.environ["ADVENT_COOKIE"])
    res = urllib.request.urlopen(req)
    encoding = res.headers.get_content_charset("utf-8")
    input = res.read().decode(encoding)
    return input

def score(fuckup: int) -> int:
    if fuckup.upper() == fuckup:
        return ord(fuckup)-38
    else:
        return ord(fuckup)-96

total = 0
for line in get_input().splitlines():
    rlen = len(line) // 2
    r1, r2 = set(line[:rlen]), set(line[rlen:])
    fuckup = r1.intersection(r2).pop()
    total += score(fuckup)

print(total)