import urllib.request
import os

def get_input() -> str:
    req = urllib.request.Request("https://adventofcode.com/2022/day/3/input")
    req.add_header("Cookie", os.environ["ADVENT_COOKIE"])
    res = urllib.request.urlopen(req)
    encoding = res.headers.get_content_charset("utf-8")
    input = res.read().decode(encoding)
    return input

def score(fuckup: int) -> int:
    if fuckup.upper() == fuckup:
        return ord(fuckup)-38
    else:
        return ord(fuckup)-96

total = 0
lines = get_input().splitlines()
for i in range(len(lines) // 3):
    group = [set(bag) for bag in lines[i*3:i*3+3]]
    badge = group[0].intersection(group[1]).intersection(group[2]).pop()
    total += score(badge)

print(total)