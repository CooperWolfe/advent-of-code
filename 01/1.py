import urllib.request
import os

def get_input() -> str:
    req = urllib.request.Request("https://adventofcode.com/2022/day/1/input")
    req.add_header("Cookie", os.environ["ADVENT_COOKIE"])
    res = urllib.request.urlopen(req)
    encoding = res.headers.get_content_charset("utf-8")
    input = res.read().decode(encoding)
    return input

elves = [0]
for line in get_input().splitlines():
    if len(line.strip()) == 0:
        elves.append(0)
    else:
        elves[-1] += int(line.strip())

print(max(elves))