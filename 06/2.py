import urllib.request
import os

def get_input() -> str:
    req = urllib.request.Request("https://adventofcode.com/2022/day/6/input")
    req.add_header("Cookie", os.environ["ADVENT_COOKIE"])
    res = urllib.request.urlopen(req)
    encoding = res.headers.get_content_charset("utf-8")
    input = res.read().decode(encoding)
    return input

chars = {}
input = get_input()
i = 0
while i < len(input):
    if chars.get(input[i]) == None:
        chars[input[i]] = i
    else:
        i = chars[input[i]]
        chars = {}
    if len(chars) == 14:
        print(i+1)
        break
    i+=1
