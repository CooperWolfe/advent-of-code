import urllib.request
import os

class Expression:
    def __init__(self, exp: str):
        self.exp = exp
    def evaluate(self, old: int) -> int:
        [lhs, op, rhs] = self.exp.replace("old", str(old)).split(" ")
        if op == "*":
            return int(lhs) * int(rhs)
        else:
            return int(lhs) + int(rhs)

class Monkey:
    def __init__(self, items: list[int], operation: Expression, selectionDivisor: int, options: tuple[int, int]):
        self.items = items
        self.operation = operation
        self.selectionDivisor = selectionDivisor
        self.options = options
        self.numInspections = 0
    def inspect(self) -> tuple[int, int]:
        item = self.items.pop(0)
        item = self.operation.evaluate(item) // 3
        self.numInspections+=1
        if item % self.selectionDivisor == 0:
            return item, self.options[0]
        else:
            return item, self.options[1]
    def append(self, item: int):
        self.items.append(item)
    def hasMoreItems(self) -> bool:
        return len(self.items) > 0

def get_input() -> str:
    req = urllib.request.Request("https://adventofcode.com/2022/day/11/input")
    req.add_header("Cookie", os.environ["ADVENT_COOKIE"])
    res = urllib.request.urlopen(req)
    encoding = res.headers.get_content_charset("utf-8")
    input = res.read().decode(encoding)
    return input

i = 0
lines = get_input().splitlines()
monkeys: list[Monkey] = []
while i < len(lines):
    if lines[i].strip().startswith("Monkey"):
        i+=1
        items = [int(value.strip(",")) for value in lines[i].strip().split(" ")[2:]]
        i+=1
        operation = Expression(" ".join(lines[i].strip().split(" ")[3:]))
        i+=1
        selectionDivisor = int(lines[i].strip().split(" ")[3])
        i+=1
        trueOpt = int(lines[i].strip().split(" ")[5])
        i+=1
        falseOpt = int(lines[i].strip().split(" ")[5])
        monkeys.append(Monkey(items, operation, selectionDivisor, options=(trueOpt, falseOpt)))
    i+=1

for i in range(20):
    for monkey in monkeys:
        while monkey.hasMoreItems():
            item, nextMonkeyIndex = monkey.inspect()
            monkeys[nextMonkeyIndex].append(item)

monkeys.sort(key=lambda monkey: monkey.numInspections, reverse=True)
print(monkeys[0].numInspections * monkeys[1].numInspections)