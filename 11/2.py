import urllib.request
import os
import functools

def parse(val: str, old: int) -> int:
    if val == "old":
        return old
    else:
        return int(val)
class Expression:
    def __init__(self, exp: str):
        [self.lhs, self.op, self.rhs] = exp.split(" ")
        self.exp = exp
    def evaluate(self, old: int) -> int:
        if self.op == "*":
            return parse(self.lhs, old) * parse(self.rhs, old)
        else:
            return parse(self.lhs, old) + parse(self.rhs, old)

class Monkey:
    def __init__(self, items: list[int], operation: Expression, selectionDivisor: int, options: tuple[int, int]):
        self.items = items
        self.operation = operation
        self.selectionDivisor = selectionDivisor
        self.options = options
        self.numInspections = 0
    def inspect(self) -> tuple[int, int]:
        item = self.items.pop(0)
        item = self.operation.evaluate(item)
        self.numInspections+=1
        if item % self.selectionDivisor == 0:
            return item, self.options[0]
        else:
            return item, self.options[1]
    def append(self, item: int):
        self.items.append(item)
    def hasMoreItems(self) -> bool:
        return len(self.items) > 0

def get_input() -> str:
    req = urllib.request.Request("https://adventofcode.com/2022/day/11/input")
    req.add_header("Cookie", os.environ["ADVENT_COOKIE"])
    res = urllib.request.urlopen(req)
    encoding = res.headers.get_content_charset("utf-8")
    input = res.read().decode(encoding)
    return input

i = 0
lines = get_input().splitlines()
monkeys: list[Monkey] = []
while i < len(lines):
    if lines[i].strip().startswith("Monkey"):
        i+=1
        items = [int(value.strip(",")) for value in lines[i].strip().split(" ")[2:]]
        i+=1
        operation = Expression(" ".join(lines[i].strip().split(" ")[3:]))
        i+=1
        selectionDivisor = int(lines[i].strip().split(" ")[3])
        i+=1
        trueOpt = int(lines[i].strip().split(" ")[5])
        i+=1
        falseOpt = int(lines[i].strip().split(" ")[5])
        monkeys.append(Monkey(items, operation, selectionDivisor, options=(trueOpt, falseOpt)))
    i+=1

# By combining all expected mods, we can keep the worry level below this number, without losing precision, per the following:
# x * y % z == (x * y % z) % z
# (x + y) % z == ((x + y) % z) % z
# if x % y == 0, z % x % y == z % y
mod_all = functools.reduce(lambda cur, acc: cur * acc, [monkey.selectionDivisor for monkey in monkeys])
for i in range(10000):
    for monkey in monkeys:
        while monkey.hasMoreItems():
            item, nextMonkeyIndex = monkey.inspect()
            monkeys[nextMonkeyIndex].append(item % mod_all)


monkeys.sort(key=lambda monkey: monkey.numInspections, reverse=True)
print(monkeys[0].numInspections * monkeys[1].numInspections)