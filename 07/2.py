import urllib.request
import os
import typing

TDir = typing.TypeVar("TDir", bound="Dir")
class Dir:
    def __init__(self, name: str, parent: TDir):
        self.name = name
        self.parent = parent
        self.children: dict[str, TDir] = {}
        self.size: int = 0
    def __str__(self):
        return self.to_string(prefix="  ")
    def to_string(self, prefix: str) -> str:
        str = f"{self.name} ({self.size}):\n"
        for child in self.children.values():
            str += prefix + child.to_string(prefix=prefix + "  ")
        return str
    def adddir(self, dir: TDir):
        self.children[dir.name] = dir
    def addfile(self, filesize: int):
        cur = self
        while cur != None:
            cur.size += filesize
            cur = cur.parent


def get_input() -> str:
    req = urllib.request.Request("https://adventofcode.com/2022/day/7/input")
    req.add_header("Cookie", os.environ["ADVENT_COOKIE"])
    res = urllib.request.urlopen(req)
    encoding = res.headers.get_content_charset("utf-8")
    input = res.read().decode(encoding)
    return input

root = Dir(name="/", parent=None)
cur = root
for line in get_input().splitlines():
    if line.startswith("$"):
        if line.startswith("$ cd"):
            if line[5:] == "/":
                cur = root
            elif line[5:] == "..":
                cur = cur.parent
            else:
                cur = cur.children[line[5:]]
    elif line.startswith("dir"):
        cur.adddir(Dir(name=line[4:], parent=cur))
    else:
        cur.addfile(int(line.split(" ")[0]))

dirs = [root]
min_size = root.size - 40000000
min_delible = 70000000
while len(dirs) > 0:
    for dir in dirs[0].children.values():
        dirs.append(dir)
    if dirs[0].size >= min_size and dirs[0].size < min_delible:
        min_delible = dirs[0].size
    dirs.pop(0)
print(min_delible)
