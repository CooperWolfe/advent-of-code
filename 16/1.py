import urllib.request
import os
import re
import typing

class Valve:
    def __init__(self, name: str, rate: int):
        self.name = name
        self.rate = rate

TStep = typing.TypeVar("TStep", bound="Step")
class Step:
    def __init__(self, valve: Valve, time_elapsed: int=0, next: TStep|None=None):
        self.valve = valve
        self.next = next
        self.num_steps = 1 if next == None else next.num_steps + 1
        self.time_elapsed = time_elapsed
        self.released = 0 if next == None else next.released + valve.rate
        self.has_tried: set[str] = set() if next == None else next.has_tried.union(valve.name)
    def can_step(self, next: str) -> bool:
        return next not in self.has_tried

def get_input() -> str:
    req = urllib.request.Request("https://adventofcode.com/2022/day/16/input")
    req.add_header("Cookie", os.environ["ADVENT_COOKIE"])
    res = urllib.request.urlopen(req)
    encoding = res.headers.get_content_charset("utf-8")
    input = res.read().decode(encoding)
    return input

valves: dict[str, Valve] = {}
valuable_valves: list[Valve] = []
lines = get_input().splitlines()
for line in lines:
    valve = Valve(name=line[6:8], rate=int(re.findall("rate=(\d+);", line)[0]))
    valves[line[6:8]] = valve
    if valve.rate > 0:
        valuable_valves.append(valve)

tunnels: dict[str, list[str]] = {}
for line in lines:
    tunnels[line[6:8]] = re.findall("valves? (.*)$", line)[0].split(", ")

shortest_path_cache: dict[tuple[str, str], int] = {}
def shortest_path(start: Valve, end: Valve) -> int:
    cached_answer = shortest_path_cache.get((start, end))
    if cached_answer != None:
        return cached_answer
    min_path_length=9999999
    paths: list[Step] = [Step(valve=start)]
    while len(paths):
        cur = paths.pop()
        if cur.num_steps > min_path_length:
            continue
        if cur.valve == end:
            min_path_length = min(min_path_length, cur.num_steps)
        else:
            for next_step in filter(lambda key: key not in cur.has_tried, tunnels[cur.valve.name]):
                paths.append(Step(valve=valves[next_step], time_elapsed=cur.time_elapsed + 1, next=cur))


max_time_elapsed = 30
paths = [Step(valves["AA"])]
max_released = 0
while len(paths):
    cur = paths.pop()
    if cur.time_elapsed > max_time_elapsed or cur.num_steps == len(valuable_valves):
        max_released = max(max_released, cur.released)
    else:
        for valve in filter(lambda v: v.name not in cur.has_tried, valuable_valves):
            cur_shortest_path = shortest_path(cur.valve, valve)
            next_step = Step(valve=valve, time_elapsed=cur.time_elapsed + cur_shortest_path + 1, next=cur)
            paths.append(next_step)
        
