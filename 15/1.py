import urllib.request
import os
import re

class Sensor:
    def __init__(self, pos: tuple[int, int], nearest_beacon_pos: tuple[int, int]):
        self.col = pos[0]
        self.nearest_beacon_pos = nearest_beacon_pos
        dist_to_beacon = abs(nearest_beacon_pos[0] - pos[0]) + abs(nearest_beacon_pos[1] - pos[1])
        dist_to_target = abs(target_row - pos[1])
        self.width_at_target = dist_to_beacon - dist_to_target

class RangeCollection:
    def __init__(self):
        self.ranges: list[tuple[int, int]] = []
    def add(self, midpoint: int, width: int):
        lhs, rhs = midpoint - width, midpoint + width 
        self.ranges.append((lhs, rhs))
        self.ranges.sort(key=lambda range: range[0])
        i = 0
        while i < len(self.ranges)-1:
            if self.ranges[i][1] >= self.ranges[i+1][0]-1:
                self.ranges[i] = (self.ranges[i][0], max(self.ranges[i][1], self.ranges[i+1][1]))
                self.ranges.remove(self.ranges[i+1])
            else:
                i+=1
    def count_cols(self) -> int:
        return sum([range[1] - range[0] + 1 for range in self.ranges])

def get_input() -> str:
    req = urllib.request.Request("https://adventofcode.com/2022/day/15/input")
    req.add_header("Cookie", os.environ["ADVENT_COOKIE"])
    res = urllib.request.urlopen(req)
    encoding = res.headers.get_content_charset("utf-8")
    input = res.read().decode(encoding)
    return input

target_row=2000000
sensors: list[Sensor] = []
lines = get_input().splitlines()
for line in lines:
    tokens = re.split("[a-zA-Z =,:]+", line)
    tokens = filter(lambda el: el != "", tokens)
    tokens = [int(el) for el in tokens]
    sensors.append(Sensor(pos=(tokens[0], tokens[1]), nearest_beacon_pos=(tokens[2], tokens[3])))

ranges = RangeCollection()
for sensor in sensors:
    if sensor.width_at_target < 0:
        continue
    ranges.add(midpoint=sensor.col, width=sensor.width_at_target)

num_not_beacon = ranges.count_cols()
target_row_beacon_cols: set[int] = set()
target_row_beacon_cols.update([sensor.nearest_beacon_pos[0] for sensor in filter(lambda sensor: sensor.nearest_beacon_pos[1] == target_row, sensors)])
num_not_beacon -= len(target_row_beacon_cols)

print(num_not_beacon)
