import urllib.request
import os
import re
from tqdm import tqdm

class Sensor:
    def __init__(self, pos: tuple[int, int], nearest_beacon_pos: tuple[int, int]):
        self.pos = pos
        self.dist_to_beacon = abs(nearest_beacon_pos[0] - pos[0]) + abs(nearest_beacon_pos[1] - pos[1])
    def dist_to_row(self, row: int) -> int:
        return abs(row - self.pos[1])
    def col(self) -> int:
        return self.pos[0]

class RangeCollection:
    def __init__(self):
        self.ranges: list[tuple[int, int]] = []
    def add(self, midpoint: int, width: int):
        lhs, rhs = midpoint - width, midpoint + width
        self.ranges.append((lhs, rhs))
        self.ranges.sort(key=lambda range: range[0])
        i = 0
        while i < len(self.ranges)-1:
            if self.ranges[i][1] >= self.ranges[i+1][0]-1:
                self.ranges[i] = (self.ranges[i][0], max(self.ranges[i][1], self.ranges[i+1][1]))
                self.ranges.remove(self.ranges[i+1])
            else:
                i+=1
    def gap(self) -> int | None:
        if len(self.ranges) > 1:
            return self.ranges[0][1]+1
        if self.ranges[0][0] > 0:
            return 0
        if self.ranges[0][1] < max_coord:
            return max_coord
        return None

def get_input() -> str:
    req = urllib.request.Request("https://adventofcode.com/2022/day/15/input")
    req.add_header("Cookie", os.environ["ADVENT_COOKIE"])
    res = urllib.request.urlopen(req)
    encoding = res.headers.get_content_charset("utf-8")
    input = res.read().decode(encoding)
    return input

sensors: list[Sensor] = []
lines = get_input().splitlines()
for line in lines:
    tokens = re.split("[a-zA-Z =,:]+", line)
    tokens = filter(lambda el: el != "", tokens)
    tokens = [int(el) for el in tokens]
    sensors.append(Sensor(pos=(tokens[0], tokens[1]), nearest_beacon_pos=(tokens[2], tokens[3])))

max_coord = 4000000
pbar = tqdm(range(max_coord+1))
for target_row in pbar:
    ranges = RangeCollection()
    for sensor in sensors:
        dist_to_target = sensor.dist_to_row(target_row)
        if sensor.dist_to_beacon < dist_to_target:
            continue
        ranges.add(midpoint=sensor.col(), width=sensor.dist_to_beacon-dist_to_target)
    gap = ranges.gap()
    if gap != None:
        pbar.close()
        print(gap*4000000+target_row)
        exit(0)