import urllib.request
import os
import typing

def sign(x: int) -> int:
    return x // abs(x)

TKnot = typing.TypeVar("TKnot", bound="Knot")
class Knot:
    def __init__(self):
        self.pos = 0, 0
    def move_up(self):
        self.pos = self.pos[0], self.pos[1] + 1
    def move_down(self):
        self.pos = self.pos[0], self.pos[1] - 1
    def move_left(self):
        self.pos = self.pos[0] - 1, self.pos[1]
    def move_right(self):
        self.pos = self.pos[0] + 1, self.pos[1]
    def move_toward(self, knot: TKnot):
        offset = knot.pos[0] - self.pos[0], knot.pos[1] - self.pos[1]
        if offset[0] != 0:
            self.pos = self.pos[0] + sign(offset[0]), self.pos[1]
        if offset[1] != 0:
            self.pos = self.pos[0], self.pos[1] + sign(offset[1])
    def is_touching(self, knot: TKnot) -> bool:
        offset = knot.pos[0] - self.pos[0], knot.pos[1] - self.pos[1]
        return abs(offset[0]) <= 1 and abs(offset[1]) <= 1

def get_input() -> str:
    req = urllib.request.Request("https://adventofcode.com/2022/day/9/input")
    req.add_header("Cookie", os.environ["ADVENT_COOKIE"])
    res = urllib.request.urlopen(req)
    encoding = res.headers.get_content_charset("utf-8")
    input = res.read().decode(encoding)
    return input

tail_visited = set([(0, 0)])
knots: list[Knot] = []
for i in range(10):
    knots.append(Knot())
lines = get_input().splitlines()
for line in lines:
    [dir, num_spaces] = line.split(" ")
    for i in range(int(num_spaces)):
        match dir:
            case "U":
                knots[0].move_up()
            case "D":
                knots[0].move_down()
            case "L":
                knots[0].move_left()
            case "R":
                knots[0].move_right()
        for i in range(1, len(knots)):
            if not knots[i].is_touching(knots[i-1]):
                knots[i].move_toward(knots[i-1])
        tail_visited.add(knots[-1].pos)

print(len(tail_visited))