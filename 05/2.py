import urllib.request
import os
import re

def get_input() -> str:
    req = urllib.request.Request("https://adventofcode.com/2022/day/5/input")
    req.add_header("Cookie", os.environ["ADVENT_COOKIE"])
    res = urllib.request.urlopen(req)
    encoding = res.headers.get_content_charset("utf-8")
    input = res.read().decode(encoding)
    return input

is_loading = True
stacks = [[], [], [], [], [], [], [], [], []]
for line in get_input().splitlines():
    if len(line.strip()) == 0:
        is_loading = False
        continue
    if is_loading:
        if not line.startswith("["):
            continue
        for i in range(9):
            box = line[i*4+1]
            if len(box.strip()) > 0:
                stacks[i].insert(0, box)
    else:
        [num, frm, to] = [int(value) for value in re.split("[a-z ]+", line) if len(value) > 0]
        boxes, stacks[frm-1] = stacks[frm-1][-num:], stacks[frm-1][:-num]
        stacks[to-1].extend(boxes)

print("".join(stack.pop() for stack in stacks if len(stack) > 0))
