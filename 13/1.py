import urllib.request
import os

def get_input() -> str:
    req = urllib.request.Request("https://adventofcode.com/2022/day/13/input")
    req.add_header("Cookie", os.environ["ADVENT_COOKIE"])
    res = urllib.request.urlopen(req)
    encoding = res.headers.get_content_charset("utf-8")
    input = res.read().decode(encoding)
    return input

def compare(lhs: int | list, rhs: int | list) -> int:
    if type(lhs) is int and type(rhs) is int:
        return lhs - rhs
    if type(lhs) is list and type(rhs) is list:
        for j in range(len(lhs)):
            if j >= len(rhs):
                return 1
            cmp = compare(lhs[j], rhs[j])
            if cmp != 0:
                return cmp
        return 0 if len(lhs) == len(rhs) else -1
    return compare([lhs], rhs) if type(lhs) is int else compare(lhs, [rhs])


lines = get_input().splitlines()
i = 0
sum = 0
while i*3 < len(lines):
    lhs: list = eval(lines[i*3])
    rhs: list = eval(lines[i*3+1])
    if compare(lhs, rhs) <= 0:
        sum += i+1
    i+=1

print(sum)