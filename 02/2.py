import urllib.request
import os

score = {
    "A": 0,
    "B": 1,
    "C": 2,
    "X": 0,
    "Y": 1,
    "Z": 2,
}

def get_input() -> str:
    req = urllib.request.Request("https://adventofcode.com/2022/day/2/input")
    req.add_header("Cookie", os.environ["ADVENT_COOKIE"])
    res = urllib.request.urlopen(req)
    encoding = res.headers.get_content_charset("utf-8")
    input = res.read().decode(encoding)
    return input

def choice(enemy: str, outcome: str) -> int:
    delta = (score[outcome] - 1) % 3
    return (score[enemy] + delta) % 3 + 1

total = 0
for line in get_input().splitlines():
    [enemy, outcome] = line.split(" ")
    total += score[outcome] * 3 + choice(enemy, outcome)

print(total)
