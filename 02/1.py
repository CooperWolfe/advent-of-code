import urllib.request
import os

score = {
    "A": 1,
    "B": 2,
    "C": 3,
    "X": 1,
    "Y": 2,
    "Z": 3,
}

def get_input() -> str:
    req = urllib.request.Request("https://adventofcode.com/2022/day/2/input")
    req.add_header("Cookie", os.environ["ADVENT_COOKIE"])
    res = urllib.request.urlopen(req)
    encoding = res.headers.get_content_charset("utf-8")
    input = res.read().decode(encoding)
    return input

def play(enemy: str, choice: str) -> int:
    cmp = score[choice] - score[enemy]
    if cmp == 0:
        return 3
    return ((cmp % 2 == 0) == (cmp < 0)) * 6

total = 0
for line in get_input().splitlines():
    [enemy, choice] = line.split(" ")
    total += score[choice] + play(enemy, choice)

print(total)
